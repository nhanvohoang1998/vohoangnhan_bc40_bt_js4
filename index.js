// Bài 1: Viết chương trình xuất 3 số theo thứ tự tăng dần 
/**
 * input: nhập 3 số nguyên
 * handle: Tìm số nhỏ nhất. So sánh số thứ 1 nếu lớn hơn số thứ 2 thì lưu giá trị số thứ nhất là số thứ 2
 *         So sánh tiếp số thứ 1 nếu lớn hơn số thứ 4 thì lưu giá trị số thứ nhất là số thứ 3
 *         Tìm số nhỏ thứ 2. So sánh số thứ 2 nếu lớn hơn số thứ 3 thì lưu giá trị số thứ 2 là số thứ 3
 * output: Sắp xếp theo thứ tự
 */

function arrange(soThu1, soThu2, soThu3) {
    var temp = null;
    if( soThu1 >= soThu2) {
        temp = soThu1;
        soThu1 = soThu2;
        soThu2 = temp;
    }

    if(soThu1 >= soThu3) {
        temp = soThu1;
        soThu1 = soThu3;
        soThu3=temp;
    }

    if (soThu2 >= soThu3) {
        temp = soThu2;
        soThu2 = soThu3;
        soThu3 = temp;
    }
    
    var chuoi = "Kết quả: " + soThu1 +"->" +soThu2 +"->" +soThu3;
    return chuoi;
}

var btnSapXep = document.getElementById('btnSapXep');
btnSapXep.addEventListener("click",function() {
    // input 
    var soThu1 = document.getElementById('soThu1').value * 1;
    var soThu2 = document.getElementById('soThu2').value * 1;
    var soThu3 = document.getElementById('soThu3').value * 1;
    
    var ketQuaSapXep = arrange(soThu1,soThu2,soThu3);

    // output
    var sapXep = document.getElementById('sapXep');
    sapXep.innerHTML = ketQuaSapXep;
});

// Bài 2: Chương trình chào hỏi
/**
 * input: chọn nhập vào bố, mẹ, anh trai, em gái
 * handle: Sử dụng switch case trường hợp bằng với các giá trị 1,2,3,4 để gửi lời chào phù hợp cho từng người
 * output: Lời chào phù hợp với từng người
 */

var guiLoiChao = document.getElementById('guiLoiChao');
guiLoiChao.addEventListener("click", function() {
    var chonThanhVien = document.getElementById('chonThanhVien').value * 1;
    var loiChao = document.getElementById('loiChao');
    
    switch(chonThanhVien) {
        case 1: {
            loiChao.innerHTML = "Kết quả: Xin chào Bố!";
            break;
        };
        case 2: {
            loiChao.innerHTML = "Kết quả: Xin chào Mẹ!";
            break;
        };
        case 3: {
            loiChao.innerHTML = "Kết quả: Xin chào Anh trai!";
            break;
        };
        case 4: {
            loiChao.innerHTML = "Kết quả: Xin chào Em gái!";
            break;
        };
        default: {
            loiChao.innerHTML = "Kết quả: Xin chào Người lạ ơi!";
        }
    };
});

//Bài 3: Đếm số chẵn lẻ
/**
 * input: nhập vào 3 số
 * handle: sử dụng biến đếm để cộng dồn đếm kết quả có bao nhiêu số chẵn và bao nhiêu số lẻ
 * output: Xuất ra bao nhiêu số chẵn, lẽ
 */

var btnDem = document.getElementById("btnDem");
btnDem.addEventListener("click", function() {
    var soThu4 = document.getElementById('soThu4').value * 1;
    var soThu5 = document.getElementById('soThu5').value * 1;
    var soThu6 = document.getElementById('soThu6').value * 1;

    var soChan = 0;
    var soLe = 0;
    if (soThu4 % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    };

    if (soThu5 % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    }

    if (soThu6 % 2 == 0) {
        soChan += 1;
    } else {
        soLe += 1;
    }

    var dem = document.getElementById("dem");
    dem.innerHTML = `Kết quả: Có ${soChan} số chẵn và ${soLe} số lẻ`
});

// Bài 4: Đoán hình tam giác 
/**
 * input: nhập độ dài 3 cạnh
 * handle:
 * output: Xuất ra tam giác vuông, cân, đều
 */
function kiemTraTamGiacVuong(soThu1, soThu2, soThu3) {
    var tmp = false
    if (soThu1 * soThu1 == soThu2 * soThu2 + soThu3 * soThu3) {
        tmp = true
    };
    if (soThu2 * soThu2 == soThu1 * soThu1 + soThu3 * soThu3) {
        tmp = true
    };
    if (soThu3 * soThu3 == soThu2 * soThu2 + soThu1 * soThu1) {
        tmp = true
    };
    return tmp;
};

var btnDuDoan = document.getElementById("btnDuDoan");
btnDuDoan.addEventListener("click", function() {
    var soThu7 = document.getElementById('soThu7').value * 1;
    var soThu8 = document.getElementById('soThu8').value * 1;
    var soThu9 = document.getElementById('soThu9').value * 1;
    var tmp = "Một loại tam giác khác";

    if(kiemTraTamGiacVuong(soThu7, soThu8, soThu9)) {
        tmp = "Hình tam giác vuông"
    }
    if(soThu7 == soThu8 || soThu7 == soThu9 || soThu8 == soThu9){
        tmp = "Hình tam giác cân"
    }
    if(soThu7 == soThu8 && soThu8 == soThu9) {
        tmp = "Hình tam giác đều"
    }

    var duDoan = document.getElementById("duDoan");
    duDoan.innerHTML = `Kết quả: ${tmp}`;

});